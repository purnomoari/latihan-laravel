@extends('layout.master')

@section('judul')
edit cast {{$cast->nama}}
@endsection
@section('content')

<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
  <div class="form-group">
    <label>nama</label>
    <input type="text" name="nama" class="form-control" value="{{$cast->nama}}" placeholder="masukan nama" >
  </div>
  @error('nama')
    <div class="alert alert-danger">{{$message}}</div>
    @enderror
  <div class="form-group">
    <label>umur</label>
    <input type="int" name="umur" class="form-control"value="{{$cast->umur}}">
  </div>
  @error('umur')
    <div class="alert alert-danger">{{$message}}</div>
  @enderror
  <div class="form-group">
    <label>bio</label>
    <textarea name="bio" class="form-control" cols="30" rows="10" value="{{$cast->bio}}"></textarea>
  </div>
  @error('bio')
    <div class="alert alert-danger">{{$message}}</div>
  @enderror
  <button type="submit" class="btn btn-primary">update</button>
</form>

@endsection