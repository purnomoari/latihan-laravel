<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', 'indexController@home');
Route::get('/Regis', 'AuthController@Register');
Route::post('/kirim', 'AuthController@kirim');

Route::get('/', function(){
    return view('welcome');
});

Route::get('/dashboard', function(){
    return view('welcome');
});
Route::get('/table', function(){
    return view('template.table');
});
Route::get('/data-tables', function(){
    return view('template.data-tables');
});

// CRUD CAST
Route::get('/cast/create', 'castController@create');
Route::post('/cast', 'castController@store');
Route::get('/cast', 'castController@index');
Route::get('/cast/{cast_id}', 'castController@show');
Route::get('/cast/{cast_id}/edit', 'castController@edit');
Route::put('/cast/{cast_id}', 'castController@update');
Route::delete('/cast/{cast_id}', 'castController@destroy');